<?php
session_start();

if (empty($_POST['id'])) {
    header('Location: /index.php');
    die();
}

require_once '../classes/Subject.php';

$subject = new Subject();
$subject->setId($_POST['id']);
$subject->delete();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Subject has been removed successfully'
];
header('Location: /index.php');