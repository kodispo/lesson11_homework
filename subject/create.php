<?php
session_start();

if (empty($_POST['name'])) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Failed to add new subject. Required fields must be specified'
    ];
    header('Location: /subject/new.php');
    die();
}

require_once '../classes/Subject.php';

$subject = new Subject($_POST['name']);
$subject->create();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'New subject has been added successfully'
];
header('Location: /index.php');