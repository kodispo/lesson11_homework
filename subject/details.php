<?php
if (empty($_GET['id'])) {
    header('Location: /index.php');
    die();
}

require_once '../classes/Subject.php';
require_once '../classes/HtmlSubjectWriter.php';

$subject = new Subject();
$html = HtmlSubjectWriter::writeDetails($subject->getById($_GET['id']));


/*
 * html output
 */
require_once '../parts/header.php';
echo $html;
require_once '../parts/footer.php';