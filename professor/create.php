<?php
session_start();

if (empty($_POST['first_name']) ||
    empty($_POST['last_name']) ||
    empty($_POST['email']) ||
    empty($_POST['department_id']) ||
    empty($_POST['subjects'])
) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Failed to add new professor. Required fields must be specified'
    ];
    header('Location: /professor/new.php');
    die();
}

require_once '../classes/Professor.php';

$professor = new Professor($_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['department_id']);
$professor->create();
$professor->setSubjects($_POST['subjects']);

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'New professor has been added successfully'
];
header('Location: /index.php');