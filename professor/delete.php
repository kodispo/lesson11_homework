<?php
session_start();

if (empty($_POST['id'])) {
    header('Location: /index.php');
    die();
}

require_once '../classes/Professor.php';

$professor = new Professor();
$professor->setId($_POST['id']);
$professor->delete();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Professor has been removed successfully'
];
header('Location: /index.php');