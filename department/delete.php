<?php
session_start();

if (empty($_POST['id'])) {
    header('Location: /index.php');
    die();
}

require_once '../classes/Department.php';

$department = new Department();
$department->setId($_POST['id']);
$department->delete();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Department has been removed successfully'
];
header('Location: /index.php');