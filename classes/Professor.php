<?php
require_once 'Core.php';
require_once 'Department.php';
require_once 'Subject.php';

class Professor extends Core {
    
    const TABLE = 'professors';
    protected $id;
    protected $first_name;
    protected $last_name;
    protected $email;
    protected $department_id;
    
    public function __construct($first_name = null, $last_name = null, $email = null, $department_id = null) {
        parent::__construct();
        $this->first_name = htmlspecialchars($first_name);
        $this->last_name = htmlspecialchars($last_name);
        $this->email = htmlspecialchars($email);
        $this->department_id = (int) htmlspecialchars($department_id);
    }
    
    public function getAll() {
        try {
            
            $sql = 'SELECT p.id, p.first_name, p.last_name, p.email, p.department_id, d.name AS department
                    FROM ' . self::TABLE . ' AS p
                    LEFT JOIN departments AS d ON d.id = p.department_id
                    ORDER BY p.id';
            $stmt = $this->pdo->query($sql);
            
            $professors = [];
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $professorArray) {
                $professor = [
                    'object' => new self(
                        $professorArray['first_name'],
                        $professorArray['last_name'],
                        $professorArray['email'],
                        $professorArray['department_id']
                    ),
                    'department' => $professorArray['department'] ?? 'not specified'
                ];
                $professor['object']->setId($professorArray['id']);
                $professors[] = $professor;
            }
            return $professors;
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to retrieve all professors!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function create() {
        try {
        
            $sql = 'INSERT INTO ' . self::TABLE . ' SET
            first_name = :first_name,
            last_name = :last_name,
            email = :email,
            department_id = :department_id';
        
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':first_name' => $this->first_name,
                ':last_name' => $this->last_name,
                ':email' => $this->email,
                ':department_id' => $this->department_id,
            ]);
            $this->id = $this->pdo->lastInsertId();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to add new professor!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getById($id) {
        $id = (int) htmlspecialchars($id);
        
        try {
            
            $sql = '
                SELECT id, first_name, last_name, email, department_id
                FROM ' . self::TABLE . '
                WHERE id = :id
            ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Professor');
            return $stmt->fetch();
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to retrieve professor information!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function update() {
        try {
        
            $sql = 'UPDATE ' . self::TABLE . ' SET
            first_name = :first_name,
            last_name = :last_name,
            email = :email,
            department_id = :department_id
            WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':id' => $this->id,
                ':first_name' => $this->first_name,
                ':last_name' => $this->last_name,
                ':email' => $this->email,
                ':department_id' => $this->department_id,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to update professor!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function delete() {
        try {
        
            $sql = 'DELETE FROM ' . self::TABLE . ' WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $this->id);
            $stmt->execute();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to remove professor!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getDepartment() {
        $departmentObject = new Department();
        return $departmentObject->getById($this->department_id);
    }
    
    public function getDepartmentName() {
        if ($department = $this->getDepartment()) {
            return $department->getName();
        } else {
            return 'not specified';
        }
    }
    
    public function getSubjects() {
        try {
            
            $sql = 'SELECT s.id, s.name FROM professor_subject AS ps
                    INNER JOIN subjects AS s ON s.id = ps.subject_id
                    WHERE professor_id = ' . $this->id;
            $stmt = $this->pdo->query($sql);
            
            $subjects = [];
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $subjectArray) {
                $subject = new Subject($subjectArray['name']);
                $subject->setId($subjectArray['id']);
                $subjects[] = $subject;
            }
            
            return $subjects;
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to retrieve professors\'s subjects!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getSubjectList() {
        if ($subjects = $this->getSubjects()) {
            $subjectList = [];
            foreach ($subjects as $subject) {
                $subjectList[] = $subject->getName();
            }
            echo implode(' | ', $subjectList);
        } else {
            return 'not specified';
        }
    }
    
    public function setSubjects($subjects) {
        try {
            
            $sql = 'DELETE FROM professor_subject WHERE professor_id = ' . $this->id;
            $this->pdo->exec($sql);
            
            $sql = 'INSERT INTO professor_subject SET
            professor_id = :professor_id,
            subject_id = :subject_id';
            $stmt = $this->pdo->prepare($sql);
            
            if (is_array($subjects) && !empty($subjects)) {
                foreach ($subjects as $subject) {
                    $stmt->execute([
                        ':professor_id' => $this->id,
                        ':subject_id' => $subject,
                    ]);
                }
            }
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to add subjects!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    
    public function getId() {
        return $this->id;
    }
    
    public function getFirstName() {
        return $this->first_name;
    }
    
    public function getLastName() {
        return $this->last_name;
    }
    
    public function getFullName() {
        return $this->first_name . ' ' . $this->last_name;
    }
    
    public function getEmail() {
        return $this->email;
    }
    
    public function getDepartmentId() {
        return $this->department_id;
    }
    
    public function setId($id) {
        $this->id = (int) htmlspecialchars($id);
    }
    
}