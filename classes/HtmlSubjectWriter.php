<?php
class HtmlSubjectWriter {
    
    static public function writeTable($subjects) {
        $html = '';
        ob_start(); ?>
    
        <h1 class="h4 mb-3 text-uppercase">Subjects</h1>
        <div class="table-responsive mb-4">
            <table class="table mb-0">
                <thead>
                <th>ID</th>
                <th>Name</th>
                <th></th>
                </thead>
                <tbody>
                <?php if (!empty($subjects)) : ?>
                    <?php foreach ($subjects as $subject) : ?>
                        <tr>
                            <td><?= $subject->getId(); ?></td>
                            <td><?= $subject->getName(); ?></td>
                            <td>
                                <div class="d-flex justify-content-end">
                                    <a href="/subject/details.php?id=<?= $subject->getId(); ?>" class="btn btn-outline-primary btn-sm mx-1">Details</a>
                                    <a href="/subject/edit.php?id=<?= $subject->getId(); ?>" class="btn btn-outline-primary btn-sm mx-1">Edit</a>
                                    <form action="/subject/delete.php" method="post">
                                        <input type="hidden" name="id" value="<?= $subject->getId(); ?>">
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="6">
                            <span class="text-danger">Empty</span>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td colspan="6" class="text-right">
                        <a href="/subject/new.php" class="btn btn-outline-primary btn-sm">Add subject</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeCreateForm() {
        $html = '';
        ob_start(); ?>
    
        <h1 class="h4 mb-3 text-uppercase">Add new subject</h1>
        <form action="/subject/create.php" method="post">
            <div class="form-group row">
                <label for="name" class="col-md-2 col-form-label">Name<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="name" name="name" placeholder="Enter name" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-10">
                    <button type="submit" class="btn btn-outline-primary">Add subject</button>
                </div>
            </div>
        </form>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeUpdateForm(Subject $subject) {
        $html = '';
        ob_start(); ?>

        <h1 class="h4 mb-3 text-uppercase">Update subject</h1>
        <form action="/subject/update.php" method="post">
            <input type="hidden" name="id" value="<?= $subject->getId(); ?>">
            <div class="form-group row">
                <label for="name" class="col-md-2 col-form-label">Name<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="name" name="name" value="<?= $subject->getName(); ?>" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-10">
                    <button type="submit" class="btn btn-outline-primary">Update subject</button>
                </div>
            </div>
        </form>
        
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeDetails(Subject $subject) {
        $html = '';
        ob_start(); ?>

        <h1 class="h4 mb-3 text-uppercase"><?= $subject->getName(); ?></h1>
        <ul class="list-group">
            <li class="list-group-item">
                <strong>Professors:</strong> <?= self::writeProfessorLinks($subject); ?>
            </li>
        </ul>
        
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeProfessorLinks(Subject $subject) {
        $professorList = [];
        if ($professors = $subject->getProfessors()) {
            foreach ($professors as $professor) {
                $professorList[] = '<a href="/professor/details.php?id=' . $professor->getId() . '">' . $professor->getFullName() . '</a>';
            }
            return implode(' | ', $professorList);
        } else {
            return 'not specified';
        }
    }
    
}