<?php
require_once 'Core.php';
require_once 'Professor.php';

class Subject extends Core {
    
    const TABLE = 'subjects';
    protected $id;
    protected $name;
    
    public function __construct($name = null) {
        parent::__construct();
        $this->name = htmlspecialchars($name);
    }
    
    public function getAll() {
        try {
        
            $sql = 'SELECT id, name FROM ' . self::TABLE;
            $stmt = $this->pdo->query($sql);
            $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Subject');
            return $stmt->fetchAll();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to retrieve all subjects!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function create() {
        try {
        
            $sql = 'INSERT INTO ' . self::TABLE . ' SET name = :name';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':name' => $this->name,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to add new subject!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getById($id) {
        $id = (int) htmlspecialchars($id);
        
        try {
            
            $sql = '
                SELECT id, name
                FROM ' . self::TABLE . '
                WHERE id = :id
            ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Subject');
            return $stmt->fetch();
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to retrieve subject information!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function update() {
        try {
        
            $sql = 'UPDATE ' . self::TABLE . ' SET
            name = :name
            WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':id' => $this->id,
                ':name' => $this->name,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to update subject!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function delete() {
        try {
        
            $sql = 'DELETE FROM ' . self::TABLE . ' WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $this->id);
            $stmt->execute();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to remove subject!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getProfessors() {
        try {

            $sql = 'SELECT p.id, p.first_name, p.last_name, p.email, p.department_id
                    FROM professor_subject AS ps
                    INNER JOIN professors AS p ON p.id = ps.professor_id
                    WHERE subject_id = ' . $this->id . '
                    ORDER BY p.first_name';
            $stmt = $this->pdo->query($sql);

            $professors = [];
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $professorArray) {
                $professor = new Professor(
                    $professorArray['first_name'],
                    $professorArray['last_name'],
                    $professorArray['email'],
                    $professorArray['department_id'],
                );
                $professor->setId($professorArray['id']);
                $professors[] = $professor;
            }

            return $professors;

        } catch (Exception $exception) {

            echo '<strong>Failed to retrieve subject\'s professors!</strong><br>' . $exception->getMessage();
            die();

        }
    }
    
    
    public function getId() {
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function setId($id) {
        $this->id = (int) htmlspecialchars($id);
    }
    
}