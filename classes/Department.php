<?php
require_once 'Core.php';

class Department extends Core {
    
    const TABLE = 'departments';
    protected $id;
    protected $name;
    protected $phone;
    
    public function __construct($name = null, $phone = null) {
        parent::__construct();
        $this->name = htmlspecialchars($name);
        $this->phone = htmlspecialchars($phone);
    }
    
    public function getAll() {
        try {
        
            $sql = 'SELECT id, name, phone FROM ' . self::TABLE;
            $stmt = $this->pdo->query($sql);
            $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Department');
            return $stmt->fetchAll();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to retrieve all departments!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function create() {
        try {
        
            $sql = 'INSERT INTO ' . self::TABLE . ' SET
            name = :name,
            phone = :phone';
        
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':name' => $this->name,
                ':phone' => $this->phone,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to add new department!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getById($id) {
        $id = (int) htmlspecialchars($id);
        
        try {
            
            $sql = '
                SELECT id, name, phone
                FROM ' . self::TABLE . '
                WHERE id = :id
            ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Department');
            return $stmt->fetch();
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to retrieve department information!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function update() {
        try {
        
            $sql = 'UPDATE ' . self::TABLE . ' SET
            name = :name,
            phone = :phone
            WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':id' => $this->id,
                ':name' => $this->name,
                ':phone' => $this->phone,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to update department!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function delete() {
        try {
        
            $sql = 'DELETE FROM ' . self::TABLE . ' WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $this->id);
            $stmt->execute();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to remove department!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    
    public function getId() {
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getPhone() {
        return $this->phone;
    }
    
    public function setId($id) {
        $this->id = (int) htmlspecialchars($id);
    }
    
}