<?php
class Core {
    
    const TABLE = null;
    protected $pdo;
    
    public function __construct() {
        $host = 'localhost';
        $dbuser = 'root';
        $password = 'root';
        $dbname = 'lesson11_homework';
    
        try {
    
            $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbname, $dbuser, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->exec('SET NAMES "utf8"');
            $this->pdo = $pdo;
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to connect to database!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function hasTable() {
        try {
        
            $sql = 'SHOW TABLES LIKE "' . self::TABLE . '"';
            $stmt = $this->pdo->query($sql);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to define if table "' . self::TABLE . '" exists!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
}