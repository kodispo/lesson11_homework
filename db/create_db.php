<?php
session_start();
require_once '../classes/Db.php';

$db = new Db();
$db->createTables();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Tables have been created successfully'
];
header('Location: /index.php');