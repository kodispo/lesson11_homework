<?php
session_start();
require_once '../classes/Db.php';

$db = new Db();
$db->populateTables();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Tables have been populated successfully'
];
header('Location: /index.php');